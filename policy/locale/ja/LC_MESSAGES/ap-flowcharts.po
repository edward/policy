# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018, 1997, 1998 Ian Jackson, Christian Schwarz, 1998-2017,
# The Debian Policy Mailing List
# This file is distributed under the same license as the Debian Policy
# Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Debian Policy Manual 4.1.6.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-07-10 16:09+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.4.0\n"

#: ../../ap-flowcharts.rst:2
msgid "Maintainer script flowcharts"
msgstr ""

#: ../../ap-flowcharts.rst:4
msgid ""
"The flowcharts [#]_ included in this appendix use the following "
"conventions:"
msgstr ""

#: ../../ap-flowcharts.rst:7
msgid "maintainer scripts and their arguments are within boxes;"
msgstr ""

#: ../../ap-flowcharts.rst:9
msgid "actions carried out external to the scripts are in italics; and"
msgstr ""

#: ../../ap-flowcharts.rst:11
msgid "the ``dpkg`` status of the package at the end of the run are in bold type."
msgstr ""

#: ../../ap-flowcharts.rst:17
msgid "Installing a package that was not previously installed"
msgstr ""

#: ../../ap-flowcharts.rst:22
msgid "Installing a package that was previously removed, but not purged"
msgstr ""

#: ../../ap-flowcharts.rst:27
msgid "Upgrading a package"
msgstr ""

#: ../../ap-flowcharts.rst:32
msgid "Removing a package"
msgstr ""

#: ../../ap-flowcharts.rst:37
msgid "Purging a package previously removed"
msgstr ""

#: ../../ap-flowcharts.rst:42
msgid "Removing and purging a package"
msgstr ""

#: ../../ap-flowcharts.rst:45
msgid ""
"These flowcharts were originally created by Margarita Manterola for the "
"Debian Women project wiki."
msgstr ""

