Format: complete
Css: README.css
Title: Debian Policy
Author: Russ Allbery, Manoj Srivastava and Sean Whitton
Email: debian-policy@lists.debian.org

# Debian Policy

## Infrastructure

+ Website:: <https://www.debian.org/doc/devel-manuals#policy>
+ Mailing list:: <debian-policy@lists.debian.org>
+ Source::
  * `git clone https://salsa.debian.org/dbnpolicy/policy`
  * Browser: <https://salsa.debian.org/dbnpolicy/policy>
+ Salsa group:: <https://salsa.debian.org/dbnpolicy> (exists
  to manage the repository but not otherwise used)

## Interacting with the team

+ Email contact:: <debian-policy@lists.debian.org>
+ Request tracker:: <https://bugs.debian.org/src:debian-policy>

Debian Policy uses a formal procedure and a set of BTS usertags to
manage the lifecycle of change proposals. For definitions of those
tags and proposal states and information about what the next step is
for each phase, see Appendix 8 of the Debian Policy Manual, "Debian
Policy changes process."

Given that most changes need to be discussed according to this
process, we have merge requests on salsa turned off.  Please submit a
bug to the BTS, either with patches attached, or a reference to a git
branch that is publicly fetchable.  See "Seeking seconds for a
patch", below, for more details.

## Contributing

### Driving change proposals

The best way to help is to find an open bug that no-one is currently
driving or shepherding, and help move it through the Policy Changes
Process.  If you're not sure whether a bug is being driven along or
shepherded by anyone, you cak ask on the debian-policy mailing list.

To find these bugs, you can use the
[list of all currently open bugs](https://bugs.debian.org/src:debian-policy),
or the view of
[bugs organised by complexity](https://bugs.debian.org/cgi-bin/pkgreport.cgi?dist=unstable;ordering=by-complexity;package=debian-policy).
A bug of simple complexity is thought to be an uncontroversial change
that probably just needs someone to write a patch.

Shepherding or driving a bug can involve:

- guiding the discussion
- seeking additional input from experts on other mailing lists
- proposing specific wording changes
- producing patches against Policy's current git HEAD
- asking parties likely to be interested to review and second patches

Most of this work can be done by people other than the Policy
delegates, and almost every change can be worked on independently of
other changes, so there's lots of opportunities for people to help.

### Translations

We have recently begun accepting translations for the Policy Manual.
Please follow the procedures of the localisation team for the language
into which you wish to translate.

When you have a translation for us to merge, it should be committed to
this repository:

    https://salsa.debian.org/dbnpolicy/policy-l10n-merge-requests-here

DDs can push to this repo's master branch; non-DDs should create merge
requests.

#### Adding a new language

1. Add the language code to the LANGUAGES variable in the Makefile.

2. Install the build-deps for debian-policy.

3. Run `make update-po`.

4. Start translating the files in locales/<code>/LC_MESSAGES/.

5. Add a new binary package for the language, or ask the Policy
   Editors to do it for you.

### Seeking seconds for a patch

When there is a consensus on the change that should be made, and you
have written a patch implementing that change (i.e. you are moving the
bug from "State C: Proposal" to "State D: Wording proposed" in the
Policy Changes Process), please follow these steps to make it as easy
as possible for others to review your work.

1. Clone policy.git, make your change, and commit it to a topic
   branch.

2. Push this branch somewhere that is publicly cloneable.  For
   example, you could fork policy.git on salsa.debian.org.

3. Address an e-mail to the bug, possibly CCing people who you think
   are likely to want to second your patch but may not be subscribed
   to the debian-policy mailing list.

4. Set the patch tag on the bug using `control: tag -1 + patch`, and
   indicate in the text of your e-mail that you are seeking seconds.

5. Include in your e-mail the output of `git diff --word-diff=plain`
   for your change.  If your change is particularly large, it might be
   more readable not to use `--word-diff=plain`, but usually the word
   diff is better.
   
    Do not quote the output -- many people have mail readers which
    will colorise the diff if it is left unmodified.

6. If you think it would be useful, optionally include in your e-mail
   instructions for how to obtain a side-by-side diff of the changes,
   by cloning your git repository.  For this, the command `git
   difftool -y -x icdiff` is useful.

7. If you think it would be useful, optionally include in your e-mail
   a URL to view a side-by-side diff online -- salsa.debian.org can do
   this.

#### Example

In this example the change is very small, so instructions for
obtaining a side-by-side diff are not really needed.  They are
included here just in order to give an example of how it can be done.

    To: 906901@bugs.debian.org

    Control: tag -1 + patch

    Hello,

    I am seeking seconds for the following patch:

    diff --git a/perl-policy.xml b/perl-policy.xml
    index 4471d68..fab2fe5 100644
    --- a/perl-policy.xml
    +++ b/perl-policy.xml
    @@ -533,7 +533,7 @@ $(MAKE) OPTIMIZE="-O2 -g -Wall"</screen>
          <title>Script Magic</title>

          <para>
            All packaged perl programs [-must-]{+should+} start with
            <literal>#!/usr/bin/perl</literal> and may append such flags as
            are required.
          </para>
    diff --git a/policy/ch-files.rst b/policy/ch-files.rst
    index f31a3b4..bc87573 100644
    --- a/policy/ch-files.rst
    +++ b/policy/ch-files.rst
    @@ -186,7 +186,7 @@ All command scripts, including the package maintainer scripts inside the
    package and used by ``dpkg``, should have a ``#!`` line naming the shell
    to be used to interpret them.

    In the case of Perl scripts this [-must-]{+should+} be ``#!/usr/bin/perl``.

    When scripts are installed into a directory in the system PATH, the
    script name should not include an extension such as ``.sh`` or ``.pl``

    =====

    To obtain a side-by-side diff:

        % git clone salsa.debian.org:someuser/policy.git debian-policy
        % cd debian-policy
        % git difftool -y -x icdiff master..origin/bug906901

    Alternatively, visit

        https://salsa.debian.org/someuser/policy/commit/d5979e6f93cfb405101e4a34960aa5b5aa9b6171?view=parallel

### Larger projects

There are also some other, larger projects:

+ Policy contains several appendices which are really documentation of
  how parts of the dpkg system works rather than technical
  Policy. Those appendices should be removed from the Policy document
  and maintained elsewhere, probably as part of dpkg, and any Policy
  statements in them moved into the main document. This project will
  require reviewing the current contents of the appendices and feeding
  the useful bits that aren't currently documented back to the dpkg
  team as documentation patches.

If you want to work on any of these projects, please mail the
debian-policy list for more information.  We'll be happy to help you
get started.

## Maintainers

The Policy Editors are official project delegates with responsibility
for maintaining, and coordinating the updating of, the Debian Policy
Manual, and all the other policy documents released as part of the
"debian-policy" package.  All of the delegates do basically the same
tasks.  The current delegates are:

+ Russ Allbery (rra)
+ Sean Whitton (spwhitton)

The Debian Policy Editors:

+ Guide the work on the Debian Policy Manual and related documents as
  a collaborative process, where developers review and second or
  object to proposals, usually on the debian-policy mailing list.
+ Count seconds and weight objections to proposals, to determine
  whether there exists a project consensus on including a change.
+ Reject, or refer to the Technical Committee, proposals that fail to
  reach consensus.
+ Commit changes to the version control system repository used to
  maintain the Debian Policy Manual and related documents.
+ Maintain the "debian-policy" package. As package maintainers, they
  have the last word on package content, releases, bug reports, etc.

Any DD can second proposed wording changes.  Everything else can be
done by anyone.  Very many other people are active on the Policy
mailing list outside of the Policy Editors.

In addition to the main technical manual, the team currently also maintains:

+ [Machine-readable debian/copyright format](https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/)
+ [Debian Menu sub-policy](https://www.debian.org/doc/packaging-manuals/menu-policy/)
+ [Debian Perl Policy](https://www.debian.org/doc/packaging-manuals/perl-policy/)
+ [Debconf Specification](https://www.debian.org/doc/packaging-manuals/debconf_specification.html)
+ [Authoritative list of virtual package names](https://www.debian.org/doc/packaging-manuals/virtual-package-names-list.yaml)

These documents are all maintained using the Policy Changes Process,
and the current state of all change proposals is tracked using the
[debian-policy BTS](https://bugs.debian.org/src:debian-policy).

## Maintenance procedures

These notes are probably of interest only to the Policy delegates.

### Repository layout

The Git repository used for Debian Policy has the following branches:

+ master:: accepted non-normative changes that will be in the next
  upload of the debian-policy package
+ next:: the current accepted normative changes that will be in the
  next release of Policy that raises at least the third component of
  the version number
+ bug<number>-<user>:: changes addressing bug <number>,
  shepherded by <user>
+ rra:: old history of Russ's arch repository, now frozen
+ srivasta:: old history of Manoj's arch repository

#### Continuous integration

On each push to the master branch, a jenkins job is triggered which builds
src:debian-policy's binary packages and installs them to
https://jenkins.debian.net/userContent/debian-policy/

### Managing a bug

Some tips for managing bugs:

+ Create a bug<number>-<user> branch for the bug, where
  <number> is the bug number in the BTS and <user>is a
  designator of the Policy team member who is shepherding the bug.
+ Commit wording changes in that branch until consensus is
  achieved. It is better not to modify debian/changelog or
  upgrading-checklist.xml during this phase. Use the BTS to track who
  proposed the wording and who seconded it.
+ Add the debian/changelog and upgrading-checklist.xml changes, and
  commit to the 'next' branch.
+ Push next out so other people may merge in their own bug branches
  without conflicts.
+ Tag the bug as pending and remove other process tags.
+ Delete the now-merged branch, both in your local repo and the remote
  on salsa.

For the debian/changelog entry, use the following format:

    * <document>: <brief change description>
      Wording: <author of wording>
      Seconded: <seconder>
      Seconded: <seconder>
      Closes: <bug numbers>

For example:

    * Policy: better document version ranking and empty Debian revisions
      Wording: Russ Allbery <rra@debian.org>
      Seconded: Raphaël Hertzog <hertzog@debian.org>
      Seconded: Manoj Srivastava <srivasta@debian.org>
      Seconded: Guillem Jover <guillem@debian.org>
      Closes: #186700, #458910

### Updating branches

After commits to master have been pushed, either by you or by another
Policy team member, you will generally want to update your working bug
branches. The equivalent of the following commands should do that:

    for i in `git show-ref --heads | awk '{print $2}'`; do
        j=$(basename $i)
        if [ "$j" != "master" ]; then
            git checkout $j && git merge master
        fi
    done
    git push --all origin

assuming that you haven't packed the refs in your repository.

### Release checklist

1. Pull any translation updates from
   `https://salsa.debian.org/dbnpolicy/policy-l10n-merge-requests-here`
   into 'next' and/or 'master'.
2. Merge 'next' to 'master' and clean up the changelog.
3. Update .po files:

    `make update-po && git commit locales -m"update po files"`

4. `dch -r` to finalise changelog
5. Update release date in the upgrading checklist
6. Bump Standards-Version in d/control
7. Commit these changes:

    `git commit debian/changelog debian/control policy/upgrading-checklist.rst -m"finalise 3.9.8.0"`

8. Make sure the package builds and installs: `dgit sbuild` or
   equivalent
9. Tag and upload: either
  - `git tag -s 3.9.8.0` followed by `dput`; or
  - `dgit push-source`
10. Push to salsa:
  - `git push --tags origin`
11. Announce on debian-devel-announce, including upgrading checklist
   section for new release
12. Also consider a more informally-worded posting on a blog that is
   syndicated to planet.debian.org.
13. Some time later, verify publication to
    https://www.debian.org/doc/debian-policy/ .
  +  This is generated by executing the following cron scripts:
      -  `/srv/www.debian.org/cron/parts/1ftpfiles` (download the deb package)
      -  `/srv/www.debian.org/cron/parts/7doc` (unpack and move pages)
  +  Its build log is available at
      -  https://www-master.debian.org/build-logs/webwml/often.log
  +  These cron scripts can be found at
      -  https://salsa.debian.org/webmaster-team/cron

### Setting release goals

Policy has a large bug backlog, and each bug against Policy tends to
take considerable time and discussion to resolve. I've found it
useful, when trying to find a place to start, to pick a manageable set
of bugs and set as a target resolving them completely before the next
Policy release. Resolving a bug means one of the following:

+ Proposing new language to address the bug that's seconded and approved by
  the readers of the Policy list following the [Policy changes process](./Progress.md)
  (or that's accepted by one of the Policy delegates if the change isn't
  normative; i.e., doesn't change the technical meaning of the document).
+ Determining that the bug is not relevant to Policy and closing it.
+ Determining that either there is no consensus that the bug indicates
  a problem, that the solutions that we can currently come up with are
  good solutions, or that Debian is ready for the change. These bugs
  are tagged wontfix and then closed after a while. A lot of Policy
  bugs fall into this category; just because it would be useful to
  have a policy in some area doesn't mean that we're ready to make
  one, and keeping the bugs open against Policy makes it difficult to
  tell what requires work. If the problem is worth writing a policy
  for, it will come up again later when hopefully the project
  consensus is more mature.

Anyone can pick bugs and work resolve them. The final determination to
accept a wording change or reject a bug will be made by a Policy
delegate, but if a patch is already written and seconded, or if a
summary of why a bug is not ready to be acted on is already written,
the work is much easier for the Policy delegate.

One of the best ways to help out is to pick one or two bugs (checking
on the Policy list first), say that you'll make resolving them a goal
for the next release, and guide the discussion until the bugs can
reach one of the resolution states above.
